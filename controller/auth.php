<?php

$pdo = require_once '../configs/connect.php';

$received_data = json_decode(file_get_contents("php://input"));
$email = $received_data->email;
$password = $received_data->password;

$data = null;

if ($received_data->action == "login") {

    try {
        $query = "select * from users where email=:umail";
        $statement = $pdo->prepare($query);
        $statement->bindParam(':umail', $email);
        $statement->execute();
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        if ($statement->rowCount() > 0) {
            if ($password == $row['password']) {
                print_r(json_encode(['status' => 200, 'data' => $row]));
            } else print_r(json_encode(['status' => 404, 'message' => 'email or password is incorrect!']));
        } else print_r(json_encode(['status' => 404, 'message' => 'no user found!']));
    } catch
    (Exception $e) {
        exit("خطایی رخ داده است : " . $e->getMessage());
    }

} else if ($received_data->action == "submit") {

    $username = $received_data->username;

    try {
        $query = "select * from users where email=:umail";
        $statement = $pdo->prepare($query);
        $statement->bindParam(':umail', $email);
        $statement->execute();
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        if ($statement->rowCount() > 0) {
            print_r(json_encode(['status' => 400, 'message' => 'duplicate email is not accessible!']));
        } else {
            $query = "insert into users (username, email, password) values (:uname, :umail, :upass)";
            $statement = $pdo->prepare($query);
            $statement->bindParam(':uname', $username);
            $statement->bindParam(':umail', $email);
            $statement->bindParam(':upass', $password);
            $statement->execute();
            $row = $statement->fetch(PDO::FETCH_ASSOC);
            print_r(json_encode(['status' => 200, 'message' => 'user register successfully']));
        }
    } catch (Exception $e) {
        exit("خطایی رخ داده است : " . $e->getMessage());
    }
}
