<?php

$pdo = require_once '../../configs/connect.php';

$name = $_POST['name'];
$description = $_POST['description'];
$price = $_POST['price'];
$discount = $_POST['discount'];
$stock = $_POST['stock'];
$image = '';
$fileUploaded = false;

if (isset($_FILES['picture']['name'])) {
    $picture = $_FILES['picture']['name'];
    $valid_extensions = array("jpg", "jpeg", "png");
    $extension = pathinfo($picture, PATHINFO_EXTENSION);
    if (in_array($extension, $valid_extensions)) {
        $up_name = 'product-' . time() . '.' . $extension;
        $upload_path = 'C:/xampp/htdocs/shop-nuxt-backend/images/' . $up_name;
        if (move_uploaded_file($_FILES['picture']['tmp_name'], $upload_path)) {
            $message = 'Image Uploaded';
            $image = $upload_path;
            $fileUploaded = true;
        } else {
            $message = 'There is an error while uploading image';
        }
    } else {
        $message = 'Only .jpg, .jpeg and .png Image allowed to upload';
    }
} else {
    $message = 'Select Image';
}

if (!$fileUploaded) {
    $output = array(
        'message' => $message,
        'image' => $image,
        'status' => 400
    );
    print_r(json_encode($output));
} else if (!isset($name)) {
    $output = array(
        'message' => 'name field can not be empty',
        'status' => 400
    );
    print_r(json_encode($output));
} else if (!isset($stock)) {
    $output = array(
        'message' => 'stock field can not be empty',
        'status' => 400
    );
    print_r(json_encode($output));
} else {
    $name_to_table = 'http://localhost/shop-nuxt-backend/images/' . $up_name;
    try {
        $query = "insert into products (`name`, `description`, `picture`, `price`, `discount`, `stock`) 
        values (:name, :description, :picture, :price, :discount, :stock)";
        $statement = $pdo->prepare($query);
        $statement->bindParam(':name', $name);
        $statement->bindParam(':description', $description);
        $statement->bindParam(':picture', $name_to_table);
        $statement->bindParam(':price', $price);
        $statement->bindParam(':discount', $discount);
        $statement->bindParam(':stock', $stock);
        $statement->execute();
        $row = $statement->fetch(PDO::FETCH_ASSOC);

        $output = array(
            'message' => 'file uploaded successfully',
            'status' => 200
        );
        print_r(json_encode($output));
    } catch (Exception $e) {
        exit("خطایی رخ داده است : " . $e->getMessage());
    }
}