<?php

$pdo = require_once '../configs/connect.php';
$data = array();
$row = new stdClass();

try {
    $query = "select * from products";
    $statement = $pdo->prepare($query);
    $statement->execute();
    $row->products = $statement->fetchAll(PDO::FETCH_ASSOC);

    $query = "select * from topSlider";
    $statement = $pdo->prepare($query);
    $statement->execute();
    $row->sliders = $statement->fetchAll(PDO::FETCH_ASSOC);

    print_r(json_encode(['status' => 200, 'data' => $row]));
} catch (Exception $e) {
    exit('خطایی رخ داده است!' . $e->getMessage());
}

