<?php

require 'config.php';

function connect($host, $db, $user, $password)
{
    try {
        $dsn = new PDO("mysql:host=" . $host . ";dbname=" . $db, $user, $password);
        $dsn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $dsn;
    } catch (PDOException $e) {
        die($e->getMessage());
    }
}

return connect(DBHOST, DBNAME, DBUSER, DBPASS);
